# ReactJS 與 Component 設計入門介紹

## ReactJS 特性簡介

- **`Learn once, write anywhere`**
- 重要特性值：
  - 基於 Component 化思考。
  - 用 JSX 進行 Declarative UI 設計。
  - 使用 Virtual DOM。
  - Component Prop Type 防呆機制。
  - Component 像是 State Machine，也有 Life Cycle。
  - Always Redraw 和 Unidirectional Data Flow。
  - Write CSS in JavaScript: Inline Style。

### 基於 Component 化思考

![component](./component.png)

- In React's world, Component is a base unit.

- Each Component contains more than one sub component.

- 並且會依照需求組裝成一個 Composable Component，因此具有 Encapsulation 、 Separation of Concerns、 Reuse、Compose 等特性。

  `<TodoApp>` 元件可以包含 `<TodoHeader />`、`<TodoList />` 子元件

  ```html
  <div>
    <TodoHeader />
    <TodoList />
  </div>
  ```

  `<TodoList />` 元件內部長相：

  ```html
  <div>
    <ul>
      <li>Write Code</li>
      <li>Sleep</li>
      <li>Buy a book</li>
    </ul>
  </div>
  ```

  元件化是最大化重複使用的關鍵，不希望重複造輪子（ DRY）

- 訓練自己看到不同網頁或應用程式的時候，強迫自己將看到的頁面切成一個個元件。

- 以下是一般 React Component 撰寫的主要兩種方式：

  - 第一種方式：
    使用 ES6 的 Class（可以進行比較複雜的操作和元件生命週期的控制，相對於 stateless components 耗費資源）

    ```javascript
    // 注意！！！！ 
    // 元件開頭第一個字母都要大寫
    class MyComponent extends React.Component {
      // render 是 Class based 元件唯一必須的方法 (method)
      render() {
        return (
        	<div> 
          		Hello, World!
          	</div>
        );
      }
    }

    // 將 <MyComponent /> 元件插入 id 為 app 的 DOM 元素中
    ReactDOM.render(<MyComponent />, document.getElementById('app'));
    ```

  - 第二種方式：
    使用 Functional Component 寫法

    - 單純地 render UI 的 stateless components
    - 沒有內部狀態、沒有實作物件和 ref ， 沒有生命週期函數。
    - 若非需要控制生命週期的話，建議多使用 stateless components 獲得比較好的效能

    ```javascript
    //使用 arrow function 來設計 Functinoal Component 讓 UI 設計 更單純
    // f(D) => UI
    // 減少 Side Effect
    const MyComponent = () => (
    	<div> Hello, World! </div>
    );
    // 將 <MyComponent /> 元件插入 id 為 app 的 DOM 元素中
    ReactDOM.render(<MyComponent />, document.getElementById('app'));
    ```

### 用 JSX 進行 Declarative UI 設計

搭配 JSX 可以實現 Declarative (What to)，而非 Imperative (How to) 的程式碼撰寫。

像 Declarative UI 設計就比單純用 Template 的方式更易懂：

```javascript
// Declarative UI Design 很容易可以看出這個 Component 的功能
<MailForm />
```

```html
// <MailForm /> 內部長相
<form>
  <input type="text" name"email" />
  <button type="submit"></button>
</form>
```

### 使用 Virtual DOM 

- 傳統 Web 中一般都是使用 jQuery 進行 DOM 的直接操作。
- 在 React 世界裡，設計有 Virtual DOM 的機制，讓 App 和 DOM 之間用 Virtual DOM 進行溝通。
- 當更改 DOM 時，會透過 React 自身的 diff 演算法去計算出最小更新，進而去最小化更新真實的 DOM。

### Component Prop Type 防呆機制

在 React 設計時，除了提供 props 預設值設定 ( Default Prop Values ) 外，也提供了 Prop 的驗證 (Validation) 機制，讓整個 Component 設計更加穩健：

```javascript
class MyComponent extends React.Component {
  render() {
    return (
    	<div> Hello, World! </div>
    );
  }
}

// PropTypes 驗證，若傳入的 props type 不符合，將會顯示錯誤
MyComponent.propTypes = {
  todo: React.PropTypes.object,
  name: React.PropTypes.string,
}

// Prop 預設值，若對應 props 沒傳入值，將會使用 default 值
MyComponent.defaultProps = {
  todo: {},
  name: '',
}
```

關於更多的 Validation 用法可以參考[官方網站](https://facebook.github.io/react/docs/reusable-components.html) 的說明。

### Component 像是 State Machine，也有 Life Cycle

Component 就像個狀態機（State Machine），根據不同的 state（透過 `setState()` 修改）和 props（由父元素傳入），Component 會出現對應的顯示結果。

#### 小小延伸閱讀

- [State and Lifecycle](https://facebook.github.io/react/docs/state-and-lifecycle.html)

### Always Redraw 和 Unidirectional Data Flow

- In React World, props and state 是影響 React Component 長相的重要要素。
- **props** 都是由父元素所傳進來，不能更改，若要更改 props 則必須由父元素進行更改。
- **state** 則是根據使用者互動而產生不同狀態，主要是透過 setState() 方法進行修改。
- 當 React 發現 props and state 更新時，會主動 Redraw 整個 UI。
  - 當然也可以使用 **forceUpdate()** 去強迫 Redraw Component。
- 透過整合 Flux and Flux-like ( Redux ) 可以更具體實現 Unidirectional Data Flow，讓資料流管理更清晰。

### Write CSS in JavaScript: Inline Style

在 React Component 中 CSS 使用 Inline Style 寫法，全都封裝在 JavaScript 當中：

```javascript
const divStyle = {
  color: 'red',
  backgroundImage: 'url(' + imgUrl + ')',
};

ReactDOM.render(
	<div style={divStyle}> Hello World! </div>,
  	document.getElementById('app')
);
```

# JSX 簡明入門教學指南

## Introduction

- React 是一個建構使用者介面的 JavaScript Library。
- 以前，總是會被灌輸許多前端分離的觀念，在前端三兄弟中： 
  - HTML 掌管內容結構
  - CSS 負責外觀樣式
  - JavaScript 主管邏輯互動
- 然而，在 React 世界裡，所有事物都是以 Component 為基礎，將同一個與 Component 相關的程式和資源都放在一起，而在撰寫 React Component 時我們通常會使用 JSX 的方式來提升撰寫效率
- JSX 並非全新語言，而是一種 Syntatic Sugar。
  - 一種類似 XML 的 ECMAScript 語法擴充。
- 與 Angular 強化 HTML 的理念也有所不同。

## 使用 JSX 的好處

### 提供更加語意化且易懂的標籤
- 如果前面有提到的，搭配 JSX 可以實現 Declarative ( How to )。

### 更加簡潔
### 結合 Native JavaScript 語法

運用 `map` 方法，輕易把 `result` 值迭代出來，產生無序清單（ul）的內容，不用再使用蹩腳的模版語言（這邊有個小地方要留意的是每個 ``元素記得加上獨特的 key 這邊用 map function 迭代出的 index，不然會出現問題）：

```javascript
  // const 為常數
  const lists = ['JavaScript', 'Java', 'Node', 'Python'];

  class HelloMessage extends React.Component {
    render() {
      return (
      <ul>
        {lists.map((result, index) => {
          return (<li key={index}>{result}</li>);
        })}
      </ul>);
    }
  }
```
## JSX 用法摘要
### 環境設定與使用方式
- 一般而言 JSX 通常有兩種使用方式：

  - 使用 [Browserify](http://browserify.org/) 或 [Webpack](https://webpack.github.io/) 等 [CommonJS](https://en.wikipedia.org/wiki/CommonJS) bundler 並整合 [babel](https://babeljs.io/) 預處理
  - 於瀏覽器端做解析。

- 一般載入 JSX 方式有：

  - 內嵌

    ```html
    <script type="text/babel">
    	ReactDOM.render(
    		<h1> Hello, World! </h1>,
    		document.getElementById('example')
    	);
    </script>
    ```

  - 從外部引入

    ```html
    <script type="text/jsx" src="main.jsx"></script>
    ```


### 標籤用法

- JSX 標籤非常類似 XML，可以直接書寫。

- 一般 Component 命名首字大寫，HTML Tags 小寫。

- Example：

  ```javascript
  class HelloMessage extends React.Component {
    render() {
      return (
      	<div>
        		<p>Hello World!</p>
        		<MessageList />
        	</div>
      );
    }
  }
  ```

### 轉換成 JavaScript
- JSX 最終會轉換成瀏覽器看得懂的 JavaScript。

- 以下為規則：
  ```javascript
  React.createElement(
  	string/ReactClass, // 表示 HTML 元素 或是 React Component
  	[object props],	// 屬性值，用物件表示
  	[children] // 接下來參數皆為元素子元素
  )
  ```
  解析前（特別注意在 JSX 中使用 JavaScript 表達式時使用 {} 括起，如下方範例的 text，裡面對應的是變數。若需希望放置一般文字，請加上 ''）：
  ```javascript
  	var text = 'Hello React';
      <h1>{text}</h1>
      <h1>{'text'}</h1>
  ```
  解析後
  ```javascript
  	var text = 'Hello React';
  	React.createElement('h1', null, text);
  	React.createElement('h1', null, 'text');
  ```

- 由於 JSX 最終會轉成 JavaScript 且每一個 JSX 節點都對應到一個 JavaScript 函數，所以在 Component 的 `render` 方法中只能回傳一個根節點（Root Nodes）。

- 例如：若有多個 <div> 要 render 請在外面包一個 Component 或 <div>、<span> 元素。

### 註解
由於 JSX 最終會編譯成 JavaScript，註解也一樣使用 // 和 /**/ 當做註解方式：
```javascript
  // 單行註解

  /*
    多行註解
  */

  var content = (
    <List>
        {/* 若是在子元件註解要加 {}  */}
        <Item
          /* 多行
             註解
             喔 */
          name={window.isLoggedIn ? window.name : ''} // 單行註解
        />
    </List>
  );
```
### 屬性

- 在 HTML 中，我們可以透過標籤上的屬性來改變標籤外觀樣式。
- 在 JSX 中也可以，但要注意 `class` 和 `for` 由於為 JavaScript 保留關鍵字用法，因此在 JSX 中使用 `className` 和 `htmlFor` 替代。
  ```javascript
    class HelloMessage extends React.Component {
      render() {
        return (
            <div className="message">
                <p>Hello World!<p>
            </div>
        );
      }
    }
  ```

#### Boolean 屬性

- 在 JSX 中預設只有屬性名稱，但沒有設值為 true。

- 例如：

  ```javascript
  // 第一個 input 標籤 disabled 雖然沒設值，但結果和下面的 input 為相同
  <input type="button" disabled />
  <input type="button" disabled={true} />

  //反之，若是沒有屬性，則預設預設為 false
  <input type="button" />
  <input type="button" disabled={false} />
  ```

### 擴展屬性

- 在 ES6 中使用 **`…`** 是迭代物件的意思。

- 可以把所有物件對應的值迭代出來設定屬性，但是要注意後面設定的屬性會蓋掉前面相同屬性

  ```javascript
  var props = {
    style: "width:20px",
    className: "main",
    value: "yo",  
  }

  <HelloMessage  {...props} value="yo" />

  // 等於以下
  React.createElement("h1", React._spread({}, props, {value: "yo"}), "Hello React!");
  ```

### 自定義屬性

- 如果在 Native HTML 元素中傳入不存在於 HTML 規範裡的屬性， React 是不會顯示的。


- 若是希望使用自定義屬性，可以使用 `data-` 當做前綴：
  ```javascript
  <HelloMessage data-custom-attr="xd" />
  ```

### 顯示 HTML

- React 會默認的進行 HTML 的轉換，避免 XSS 攻擊。

- 有許多繞過的方法，最簡單的方法就是直接用 Unicode 字符。

  - 需確保文件是 UTF-8 編碼且網頁也需要指定為 UTF-8 編碼。

    ```javascript
    <div>{'First · Second'}</div>
    ```

- 但是安全的作法是，先找到 實體的 Unicode 編碼，然後在 JavaScript 中使用。

  ```javascript
  <div>{'First \u00b7 Second'}</div>
  <div>{'First ' + String.fromCharCode(183) + ' Second'}</div>
  ```

- 雖然還可以直接使用原生 HTML ，但是不推薦這樣的做法：

  ```javascript
  <div dangerouslySetInnerHTML={{__html: 'First &middot; Second'}} />
  <div dangerouslySetInnerHTML={{__html: '<h1>Hello World!!</h1>'}}></div>
  ```

- 更多可以參考 [這篇](https://segmentfault.com/a/1190000007060623#articleHeader11)

### 樣式使用

```javascript
const divStyle = {
  color: 'blue',
  backgroundImage: 'url(' + imgUrl + ')',
};

function HelloWorldComponent() {
  return <div style={divStyle}>Hello World!</div>;
}
```

Support older browsers, u need to supply corresponding style properties：

```javascript
const divStyle = {
  WebkitTransition: 'all', // note the capital 'W' here
  msTransition: 'all' // 'ms' is the only lowercase vendor prefix
};

function ComponentWithTransition() {
  return <div style={divStyle}>This should work cross-browser</div>;
}
```

#### 樣式使用 - 延伸閱讀
- [官方文件 - style](https://facebook.github.io/react/docs/dom-elements.html#style)
- [camelCased (駝峰式)](https://zh.wikipedia.org/wiki/駝峰式大小寫)

### 事件處理

- [官方文件 - Supported Events](https://facebook.github.io/react/docs/events.html#supported-events)

### 延伸閱讀
- [Airbnb React/JSX Style Guide](https://github.com/airbnb/javascript/tree/master/react)
- [JSX 展开属性](http://reactjs.cn/react/docs/jsx-spread-zh-CN.html)
- [[react学习系列之深入jsx](https://segmentfault.com/a/1190000007060623)](https://segmentfault.com/a/1190000007060623)
- [Imperative programming or declarative programming](http://www.puritys.me/docs-blog/article-320-Imperative-programming-or-declarative-programming.html)
- [JSX in Depth](https://facebook.github.io/react/docs/jsx-in-depth.html)