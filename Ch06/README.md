# ImmutableJS 入門教學
![immutablejs](./immutable.png)

## 前言
- JavaScript 中有兩種資料型態

  - **Primitive**
    - String
    - Number
    - Boolean
    - null
    - undefinded
  - **Object**
    - Reference

- 雖然操作起來比 Java 容易很多，但也相對不嚴謹。

- 由於 JavaScript 中的 Object 資料是 Mutable，由於是使用 Reference 的方式，所以修改到複製的值也會修改到原始值。

  - For example：

    ```javascript
    // map2 值是指到 map1，所以當 map1 值一改
    // map2 的值也會受到影響。
    var map1 = { a: 1};
    var map2 = map1;
    map2.a = 2;
    ```

  - 所以通常一般做法是使用 [**`deepCopy`**](http://web.jobbole.com/87627/) 來避免修改，不過這樣子的作法卻產生較多的資源浪費（每次的 deep-copy 都要把整個對象遞迴的複製一份）。

- 為了解決這個問題，在 2013 年時 Facebook 工程師 Lee Byron 打造了 [ImmutableJS](https://facebook.github.io/immutable-js/)，但並沒有被預設放到 React 工具包中（雖然有提供簡化的 Helper），但 `ImmutableJS` 的出現確實解決了 `React` 甚至 `Redux` 所遇到的一些問題。

## 什麼是 Immutable Data
- 所謂的 Immutable Data 就是一旦建立，就不能再被修改的數據資料。
- 對 Immutable 對象的任何修改或新增甚至是刪除的動作，都會返回一個新的 Immutable 對象。
- 原理是 **Persistent Data Structure**，也就是使用舊有的數據創見新的數據時，要保證舊數據同時可用且不變。
- 也為了避免 deepCopy 把所有節點都複製一遍帶來的性能消耗，Immutable 更使用了 Structural Sharing (共享）。
  - 也就是說如果對象的 tree 中任何一個 node 發生變化，它只修改這個 node 和受它影響的 parent node，其他 node 則進行共享。（參考 [Immutable 详解及 React 中实践](https://github.com/camsong/blog/issues/3) 中的一個動畫）
  - ![immutable_animation.gif](./immutable_animation.gif)

## ImmutableJS 特性
### 介紹
- ImmutableJS 提供了 7 種不可修改的資料類型
  - List
  - Map
  - Stack
  - OrderedMap
  - Set
  - OrderedSet
  - Record

- 若是對 Immutable 物件操作，都會回傳一個新值。

- 其中有三種最重要也最常用的資料結構：

  - Map：類似於 key/value 的 object，在 ES6 也有原生 `Map` 對應
    ```javascript
    import Immutable from 'immutable';
    // 1. Map Size
    const Map = Immutable.Map;
    const map1 = Map({a: 1});
    console.log(map1.size);
    // => 1
    //
    // 2. Add or Replace Map elements
    // set(key: K, value: V)
    const map2 = map1.set('a', 7);
    console.log('map2:'+map2);
    // => Map { "a": 7 }
    //
    // 3. Delete Elements
    // delete(key: k)
    const map3 = map1.delete('a');
    console.log('map3:'+map3);
    // => Map {}
    //
    // 4. Clear Map contents
    const map4 = map1.clear();
    console.log('map4:'+map4);
    // => Map {}
    //
    // 5. 更新 Map 元素
    // update(updater: (value: Map<K, V>) => Map<K, V>)
    // update(key: K, updater: (value: V) => V)
    // update(key: K, notSetValue: V, updater: (value: V) => V)
    const map5 = map1.update('a', () => (7))
    console.log('map5:'+map5);
    // => Map { "a": 7 }
    //
    // 6. 合併 Map 
    const map6 = Map({ b: 3 });
    const map7 = map1.merge(map6);
    console.log('map7'+map7);
    // => Map { "a": 1, "b": 3 }
    ```

  - List：有序且可以重複值，對應於一般的 Array

    ```javascript
    import Immutable from 'immutable';
    const List Immutable.List;
    // 1. Get List Length
    const arr1 = List([1, 2, 3]);
    console.log(arr1.size);
    // => 3
    //
    // 2. Add or Replace List contents
    // set(index: number, value T)
    // 將 index 位置的元素替換
    const arr2 = arr1.set(-1, 7);
    console.log('arr2:'+arr2);
    // => [1, 2, 7]
    //
    const arr3 = arr1.set(4, 0);
    console.log('arr3:'+arr3);
    // => [1, 2, 3, undefined, 0]
    //
    // 3. 刪除 List 元素
    // delete(index: number)
    // 刪除 index 位置的元素
    const arr4 = arr1.delete(1);
    console.log('arr4:'+arr4);
    // => [1, 3]
    //
    // 4. 插入元素到 List
    // insert(index: number, value: T)
    // 在 index 位置插入 value
    const arr5 = arr1.insert(1, 2);
    console.log('arr5:'+arr5);
    // => [1, 2, 2, 3]
    //
    // 5. 清空 List
    // clear()
    const arr6 = arr1.clear();
    console.log('arr6:'+arr6);
    // => []
    ```

  - Set：沒有順序且不能重複的列表

    ```javascript
    import Immutable from 'immutable';
    const Set= Immutable.Set;
    // 1. 建立 Set
    const set1 = Set([1, 2, 3]);
    console.log('set1:'+set1);
    // => Set { 1, 2, 3 }
    //
    // 2. 新增元素
    const set2 = set1.add(1).add(5);
    console.log('set2:'+set2);
    // => Set { 1, 2, 3, 5 } 
    // 由於 Set 為不能重複集合，故 1 只能出現一次
    //
    // 3. 刪除元素
    const set3 = set1.delete(1);
    console.log('set3:'+set3);
    // => Set { 3, 2 }
    //
    // 4. 取聯集
    const set4 = Set([2, 3, 4, 5, 6]);
    const set5 = set1.union(set4);
    console.log('set5:'+set5);
    // => Set { 1, 2, 3, 4, 5, 6 }
    //
    // 5. 取交集
    const set6 = set1.intersect(set4);
    console.log('set6:'+set6);
    // => Set { 2, 3 }
    //
    // 6. 取差集
    const set7 = set1.subtract(set4);
    console.log('set7:'+set7);
    // => Set { 1 }
    ```
### 整理
- **Persistent Data Structure** 在 `ImmutableJS` 的世界裡，只要資料一被創建，就不能修改，維持 `Immutable`。
  - 進而就不會發生下列的狀況：
    ```javascript
    function functionA(obj) {
        obj.a = 2;
    }
    var obj = {
        a: 1,
    };
    console.log(obj.a);
    // => 還是 1
    functionA(obj);
    console.log(obj.a);
    // 進去 functionA 後，就不一定還是 1 了
    ```
  - 使用 `ImmutableJS` 後：
    ```javascript
    function functionA(obj) {
      obj.set('a',6);
      const obj2 = obj.set('a',6);
      console.log('obj2: '+obj2);
      // => obj2: Map { "a": 6 }
    }
    // 有些開發者在使用時會在 ``Immutable` 變數前加 `$` 以示區隔。
    var $obj = Immutable.fromJS({
    	a: 1,
    });
    console.log('obj: '+$obj);
    // => obj: Map { "a": 1 }
    functionA($obj);
    console.log('obj: '+$obj);
    // => obj: Map { "a": 1 }
    ```

- **Structural Sharing** 為了維持資料的不可變，又要避免像 **`deepCopy`** 一樣複製所有的節點資料而造成的資源損耗。

- 在 **`ImmutableJS`** 使用的是 **Structural Sharing** 特性，亦即如果物件樹中一個節點發生變化的話，只會修改這個節點和和受它影響的父節點，其他節點則共享。
  ```javascript
  const obj = {
    count: 1,
    list: [1, 2, 3, 4, 5]
  }
  var $map1 = Immutable.fromJS(obj);
  var $map2 = $map1.set('count', 4);

  console.log($map1.list === $map2.list); 
  // ==> true
  ```

- [**Support Lazy Operation**](http://zhenhua-lee.github.io/react/Immutable.html#support-lazy-operation)

  - ImmutableJS借鑑了 Clojure、Scala、Haskell ..等，这些 Functional programming language，引入了一個特殊的結構 **`Seq (Sequence)`**， 其他Immutable的對象（例如`List`、`Map`）可以通過 **`toSeq`** 進行轉換。
  - **`Seq`** 具有兩個特性：**Immutable**、**Lazy**。

- 豐富的 API 並提供快速轉換原生 JavaScript 的方式 在 ImmutableJS 中可以使用 `fromJS()`、`toJS()` 進行 JavaScript 和 ImmutableJS 之間的轉換。

  - 由於在轉換之間會非常耗費資源， **所以若是你決定引入 `ImmutableJS` 的話請盡量維持資料處在 `Immutable` 的狀態** 。
  - [强大的API机制](http://zhenhua-lee.github.io/react/Immutable.html#api)

- 支持 Functional Programming `Immutable` 本身就是 Functional Programming 的概念，所以在 `ImmutableJS` 中可以使用許多 Functional Programming 的方法。

  - 例如：`map`、`filter`、`groupBy`、`reduce`、`find`、`findIndex` 等。

- 容易實現 Redo/Undo 歷史回顧

## React 效能優化

- `ImmutableJS` 除了可以和 `Flux/Redux` 整合外，也可以用於基本 react 效能優化。

  - 以下是一般使用效能優化的簡單方式：

    - 傳統 JavaScript 比較方式，若資料型態為 Primitive 就不會有問題：

      ```javascript
      // 在 shouldComponentUpdate 比較接下來的 props 是否一致，若相同則不重新渲染，提昇效能
      shouldComponentUpdate (nextProps) {
          return this.props.value !== nextProps.value;
      }
      ```

    - 如果是比較 Object 的話，就會有問題：

      ```javascript
      // 假設 this.props.value 為 { foo: 'app' }
      // 假設 nextProps.value 為 { foo: 'app' },
      // 雖然兩者值是一樣，但由於 reference 位置不同，所以視為不同。但由於值一樣應該要避免重複渲染
      this.props.value !== nextProps.value; // true
      ```

    - 使用 `ImmutableJS`：

      ```javascript
      var SomeRecord = Immutable.Record({ foo: null });
      var x = new SomeRecord({ foo: 'app'  });
      var y = x.set('foo', 'azz');
      x === y; // false
      ```

    - 在 ES6 中可以使用官方文件上的 `PureRenderMixin` 進行比較，可以讓程式碼更簡潔：
      ```javascript
      import PureRenderMixin from 'react-addons-pure-render-mixin';
      class FooComponent extends React.Component {
        constructor(props) {
          super(props);
          this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
        }
        render() {
          return <div className={this.props.className}>foo</div>;
        }
      }
      ```
- 更多請閱讀：
  - [React 巢狀 Component 效能優化](https://blog.wuct.me/react-%E5%B7%A2%E7%8B%80-component-%E6%95%88%E8%83%BD%E5%84%AA%E5%8C%96-b01d8a0d3eff#.3kf4h1xq1)


## 延伸閱讀

- [Javascript 深拷贝](http://web.jobbole.com/87627/)
- [[Javascript] 關於 JS 中的淺拷貝和深拷貝](http://larry850806.github.io/2016/09/20/shallow-vs-deep-copy/)
- [React应用性能优化之IMMUTABLE.JS](http://robin-front.github.io/2016/03/14/React%E5%BA%94%E7%94%A8%E6%80%A7%E8%83%BD%E4%BC%98%E5%8C%96%E4%B9%8BIMMUTABLE-JS/)
- [facebook immutable.js 意义何在，使用场景？](https://www.zhihu.com/question/28016223)
- [Immutable 详解及 React 中实践](https://github.com/camsong/blog/issues/3)
- [Immutable.js及在React中的应用](http://zhenhua-lee.github.io/react/Immutable.html)
- [PureRenderMixin](https://facebook.github.io/react/docs/pure-render-mixin.html)
- [React 巢狀 Component 效能優化](https://blog.wuct.me/react-%E5%B7%A2%E7%8B%80-component-%E6%95%88%E8%83%BD%E5%84%AA%E5%8C%96-b01d8a0d3eff#.3kf4h1xq1)