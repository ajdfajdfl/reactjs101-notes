# React 開發環境設置與 Webpack 入門教學
> 本章接下來將討論 React 開發環境的兩種主要方式：CDN-based、 [webpack](https://webpack.github.io/)（這邊我們就先不討論 [TypeScript](https://www.typescriptlang.org/) 的開發方式）。
>
> 至於 [browserify](https://webpack.github.io/) 搭配 [Gulp](http://gulpjs.com/) 的方法則會放在補充資料中

## JavaScript 模組化

> 更多參考：
>
> - [JavaScript 模块化*七日谈*](https://huangxuan.me/2015/07/09/js-module-7day/)
> - [JavaScript Modules: A Beginner’s Guide](https://medium.freecodecamp.com/javascript-modules-a-beginner-s-guide-783f7d7a5fcc#.pi27l5exd)

### 傳統 CDN-based

```HTML
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Hello React!</title>
    <!-- 以下引入 react.js, react-dom.js（react 0.14 後將 react-dom 從 react 核心分離，更符合 react 跨平台抽象化的定位）以及 babel-core browser 版 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/react/15.2.1/react.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/react/15.2.1/react-dom.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-standalone/6.18.1/babel.min.js"></script>
  </head>
  <body>
    <!-- 這邊的 id="example" 的 <div> 為 React Component 要插入的地方 -->
    <div id="example"></div>
    <!-- 以下就是包在 babel（透過進行語言翻譯）中的 React JSX 語法，babel 會將其轉為瀏覽器看的懂得 JavaScript -->
    <script type="text/babel">
      ReactDOM.render(
        <h1>Hello, world!</h1>,
        document.getElementById('example')
      );
    </script>
  </body>
</html>
```

- 理解 React 是 Component 導向的應用程式設計

- 引入 react.js, react-dom.js and babel-standalone 版的 script

- 在 body 內撰寫 React Component 指定節點的地方

  ```html
  <div id="example"></div>
  ```

- 透過 babel 進行轉譯 React JSX 的語法， babel 會將 JSX 轉為 Browser 看得懂的 JavaScript。

## Webpack

![webpack](./webpack-module-bundler.png)

上個段落簡單講了 CDN-based 的開發方式，由於用 CDN-based 的開發方式有不少缺點。

所以 Webpack 會是接下來主要使用的開發工具。

誠如[上一章節](../Ch01/README.md)所介紹的，[Webpack](https://webpack.github.io/) 是一個模組打包工具（module bundler）：

- 將 CSS, Pictures  and other resource 做打包的動作。
- 打包之前預處理檔案（Less, CoffeeScript, JSX, ES6...等）。
- 依 Entry 文件不同，把 .js 分拆為多個 .js 檔案。

### 建置

- 依據作業系統安裝  [Node](https://nodejs.org/en/) 和 [NPM](https://www.npmjs.com/) 。

- 透過 npm 安裝 webpack, webpack loader, webpack-dev-server

  - webpack 中的 loader 類似於 browserify 內的 transforms ， 但 Webpack 在使用上比較多元。

  - 除了 JavaScript loader 外也有 CSS Style 和圖片的 loader。

  - `webpack-dev-server` 則可以啟動開發用 Server，方便測試使用。

    ```shell
    // 透過 npm 初始化，設定 package.json
    $npm init
    // --save-dev 可以將安裝套件的名稱和版本資訊直接存放到 package.json，方便日後使用。
    $npm install --save-dev babel-core babel-eslint babel-loader babel-preset-es2015 babel-preset-react html-webpack-plugin webpack webpack-dev-server
    ```

- 在根目錄設定 `webpack.config.js`

  - `webpack.config.js` 有點類似於 `gulp` 中的 `gulpfile.js` 功用。

  ```javascript
  // 這邊使用 HtmlWebpackPlugin，將 bundle 好的 <script> 插入到 body。
  //$(__dirname) 為 ES6 語法對應到 __dirname
  const HtmlWebpackPlugin = require('html-webpack-plugin');

  const HTMLWebpackPluginConfig = new HtmlWebpackPlugin({
    template: `$(__dirname)/app/index.html`,
    filename: 'index.html',
    inject: 'body',
  });

  module.exports = {
    //檔案起始點從 entry 進入，因為是陣列所以也可以是多個檔案
    entry: [
      '.app/index.js',
    ],
    output: {
      path: `$(__dirname)/dist`,
      filename: `index_bundle.js`,
    },
    module: {
      // loaders 則是放欲使用的 loaders，在這邊是使用 babel-loader 將所有 .js（這邊用到正則式）相關檔案（排除了 npm 安裝的套件位置 node_modules）轉譯成瀏覽器可以閱讀的 JavaScript。preset 則是使用的 babel 轉譯規則，這邊使用 react、es2015。若是已經單獨使用 .babelrc 作為 presets 設定的話，則可以省略 query
      loaders: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loaders: 'babel-loader',
          query: {
            presets: ['es2015', 'react'],
          },
        },
      ],
    },
    //devServer 則是 webpack-dev-server 設定
    devServer:{
      inline: true,
      port: 8000,
    },
    // plugins 放置所使用的外掛
    plugins: [HTMLWebpackPluginConfig],
  };
  ```

- 在根目錄設定 `.babelrc`

  ```javascript
  {
    "presets": [
      "es2015",
      "react",
    ],
     "plugins": []
  }
  ```

- 安裝 react 和 react-dom

  ```shell
  $ npm install --save react react-dom
  ```

- 撰寫 Component `index.html`

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>React Setup</title>
      <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  </head>
  <body>
      <!-- 欲插入 React Component 的位置 -->
      <div id="app"></div>
  </body>
  </html>
  ```

  `index.js`

  ```javascript
  import React from 'react';
  import ReactDOM from 'react-dom';

  class App extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
      };
    }
    render() {
      return (
        <div>
          <h1>Hello, World!</h1>
        </div>
      );
    }
  }

  ReactDOM.render(<App />, document.getElementById('app'));
  ```

- 在終端機使用 `webpack` 進行成果展示，webpack 相關指令：

  - webpack：會在開發模式下開始一次性的建置
  - webpack -p：會建置 production 的程式碼
  - webpack --watch：會監聽程式碼的修改，當儲存時有異動時會更新檔案
  - webpack -d：加入 source maps 檔案
  - webpack --progress --colors：加上處理進度與顏色

  如果不想每次都打一長串的指令碼的話可以使用 `package.json` 中的 `scripts` 設定

  ```json
  "scripts": {
    "dev": "webpack-dev-server --devtool eval --progress --colors --content-base build"
  }
  ```

  然後在終端機執行：

  ```shell
  $ npm run dev
  ```

## 其他延伸閱讀

1. [JavaScript 模块化七日谈](http://huangxuan.me/2015/07/09/js-module-7day/)
2. [前端模块化开发那点历史](https://github.com/seajs/seajs/issues/588)
3. [Webpack 中文指南](http://zhaoda.net/webpack-handbook/index.html)
4. [WEBPACK DEV SERVER](http://webpack.github.io/docs/webpack-dev-server.html)