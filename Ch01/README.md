# Web 前端工程入門簡介

## 前端工程範疇
現在的 Web 平台已經不再侷限於桌面瀏覽器，而是必須面對更多的跨平台、跨瀏覽器的應用開發場景，其中包含：
1. 網頁瀏覽器（Web Browser），一般的網頁應用程式開發
2. 透過 CLI 指令去操作的 Headless 瀏覽器（Headless　Application）。例如：[phantomJS](http://phantomjs.org/)、[CasperJS](http://casperjs.org/) 等
3. 運作在 WebView 瀏覽器核心（WebView Application）的應用。例如：[Apache Cordova](https://cordova.apache.org/)、[Electron](http://electron.atom.io/)、[NW.js](http://nwjs.io/) 等行動、桌面應用程式開發
4. 原生應用程式（Native Application），透過 Web 技術撰寫原生應用程式。例如：[React Native](https://facebook.github.io/react-native/)、[Native Script](https://www.nativescript.org/) 等

## React 生態系（Ecosystem）入門簡介

### ReactJS 是什麼
- 根據 [React 官方網站](https://facebook.github.io/react/) 的說明：React 是一個專注於 UI（View）的 JavaScript 函式庫（Library）。
- ReactJS 是 Facebook 推出的 JavaScript 函式庫，若以 MVC 框架來看，React 定位是在 View 的範疇。
    - 在 ReactJS 0.14 版之後，ReactJS 更把原先處理 DOM 的部分獨立出去（react-dom），讓 ReactJS 核心更單純，也更符合 React 所倡導的 **_Learn once, write everywhere_** 的理念。
      - React and React DOM
        - One is the actual library and the second one is for manipulating the DOM, which now you can describe in JSX.
        - Refer to [How it feels to learn JavaScript in 2016](https://hackernoon.com/how-it-feels-to-learn-javascript-in-2016-d3a717dd577f#.6879hghth)

### FEW THINGS NEED TO KNOW

#### JSX 是什麼
- JSX is just a JavaScript syntax extension that looks pretty much like XML. It’s kind of another way to describe the DOM, think of it as a better HTML.
    - - Refer to [How it feels to learn JavaScript in 2016](https://hackernoon.com/how-it-feels-to-learn-javascript-in-2016-d3a717dd577f#.6879hghth)
- JSX 是一種 [Syntactic Sugar](https://en.wikipedia.org/wiki/Syntactic_sugar)，一種語法類似 XML 的 ECMAScript 語法擴充。

##### 使用 JSX 的好處
-   提供更加語意化
    - 類似 XML 語法，例如做一個回饋表單，使用 HTML 寫法通常會長這樣：
      ```html
            <form class="messageBox">
              <textarea></teextarea>
              <button type="submit"></button>
            </from>
      ```
      使用上是：
      ```xml
            <MessageBox />
      ```
    - React 思路認為使用 Component 比起模版（Template）和顯示邏輯（Display Logic）更能實現關注點分離的概念，而搭配 JSX 可以實現聲明式 Declarative（注重 what to），而非命令式 Imperative（注重 how to）的程式撰寫方式。

    以一個 Facebook 點讚的功能來說：
    -   Declarative
        ```javascript
            if(this.state.liked) {
              return (<BlueLike />);
            }else {
              return (<GrayLike>);
            }
        ```
    -   Imperative
        ```javascript
                    if(userLike()) {
                      if(!hasBlueLike()) {
                        removeGrayLike();
                        addBlueLike();
                      }
                    }else {
                      if(hasBlueLike()) {
                        removeBlueLike();
                        addGrayLike();
                      }
                    }
        ```

-   更加簡潔

-   結合原生 JavaScript 語法

##### 延伸閱讀
- [一看就懂的 JSX 簡明入門教學指南](http://blog.techbridge.cc/2016/04/21/react-jsx-introduction/)
- [語意化 (Semantics)](https://intersectiontw.github.io/maintainablecss/chapters/semantics/)

#### Babel 是什麼
- 由於並非所有瀏覽器都支援 ES6+ 語法，所以透過 [Babel](https://babeljs.io/) 這個 JavaScript 編譯器（可以想成是翻譯機或是翻譯蒟篛）可以讓你的 ES6+ 、JSX 等程式碼轉換成瀏覽器可以看的懂得語法。通常會在資料夾的 root 位置加入 .bablerc 進行轉譯規則 preset 和引用外掛（plugin）的設定。
- More can read: [Babel 入门教程 - 阮一峰的网络日志](http://www.ruanyifeng.com/blog/2016/01/babel.html)

#### ES6+ 是什麼
- [ES6+ ](https://babeljs.io/blog/2015/06/07/react-on-es6-plus) 是指 ES6 (ES2015) 和 ES7 的聯集，在 ES6+ 新的標準中引入了許多新的特性和功能，彌補了過去 JavaScript 被詬病的一些特性。
- More can read: [一看就懂的 React ES5、ES6+ 常見用法對照表](http://blog.kdchang.cc/2016/04/04/react-react-native-es5-es6-cheat-sheet/)

#### NPM 是什麼
- NPM（Node Package Manager）是 Node.js 下的主流套件管理工具。
- 它是基於 [CommonJS](https://en.wikipedia.org/wiki/CommonJS) 的規範，通常必須搭配 Browserify 這樣的工具才能在前端使用 NPM 的模組。
    - 然而因 NPM 是基於 Nested Dependency Tree，不同的套件有可能會在引入依賴時會引入相同但不同版本的套件，造成檔案大小過大的情形。這和另一個套件管理工具 [Bower](https://bower.io/) 專注在前端套件且使用 Flat Dependency Tree（讓使用者決定相依的套件版本）是比較不同的地方

#### JavaScript 模組化開發

- 什麼是模組化？
  > 解耦的重要性

  - 通常提到一個應用是模組化的時候，我們通常指它是由一組高度解耦的、存放在不同模組中的獨特功能構成。
- 以下簡單介紹 JavaScript 模組化的相關規範。事實上，在一開始沒有官方定義的標準時出現了各種社群自行定義的規範和實踐。
  - CDN-Based
    - 也就是傳統的 \<script\> 引入方式。
    - 雖然使用方便，但是在開發大型應用程式時，會產生許多弊端：
      -  在 Global Scope 容易造成 [變數污染](http://kuro.tw/posts/2015/07/08/note-javascript-variables-declared-with-the-scope-scope/) 和 衝突。
      -  文件只能依照 \<script\> 順序依序載入，沒什麼彈性。
      -  必須由開發者自行判斷各個模組和函式庫之間的的依賴關係。
      -  在大型專案中，各種資源和版本難以維護。

  - AMD (Asynchronous Module Definition)
    > 一種在瀏覽器中編寫模組化 JavaScript 的格式

    - 誕生於 [Dojo toolkit](https://en.wikipedia.org/wiki/Dojo_Toolkit) 在使用 [XHR](https://developer.mozilla.org/zh-TW/docs/Web/API/XMLHttpRequest) + [eval](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/eval) 時的實踐經驗。

    - 一種非同步載入模組的規範，在宣告模組時即需定義依賴的模組。

    - 兩個重要的觀念：

      - 一個是進行模組定義的 define 方法

        ```javascript
        define(
        	module_id /*optional*/,
          	[dependencies] /*optional*/,
        	definition function /*用來初始化模組或
        						對象的函數*/
        );
        ```

      - 另一個是用來處理 Dependency 加載的 require 方法

  - CommonJS

    - [CommonJS](http://wiki.commonjs.org/wiki/CommonJS) 規範是一種同步模組載入的規範。
    - 以 Node.js 其遵守 CommonJS 規範，使用 `require` 進行模組同步載入，並透過 `exports`、`module.exports` 來輸出模組。主要實現為 [Node.js](https://nodejs.org/en/) 伺服器端的同步載入和瀏覽器端的 [Browserify](http://browserify.org/)。

  - CMD

  - UMD

  - ES6 Module

    - CMAScript6 的標準中定義了 JavaScript 的模組化方式，讓 JavaScript 在開發大型複雜應用程式時上更為方便且易於管理，亦可以取代過去 AMD、CommonJS 等規範，成為通用於瀏覽器端和伺服器端的模組化解決方案。
    - 但目前瀏覽器和 Node 在 ES6 模組支援度還不完整，大部分情況需要透過 [Babel](https://babeljs.io/) 轉譯器進行轉譯。

##### 延伸閱讀
- [使用 AMD、CommonJS 及 ES Harmony 编写模块化的 JavaScript](http://justineo.github.io/singles/writing-modular-js/)
- [前端模块化开发的价值](https://github.com/seajs/seajs/issues/547)
- [JavaScript 模块的循环加载](http://www.ruanyifeng.com/blog/2015/11/circular-dependency.html)
- [浅谈模块化加载的实现原理](http://www.cnblogs.com/hustskyking/p/how-to-achieve-loading-module.html)

#### Webpack/Browserify + Gulp
-   隨著網頁應用程式開發的複雜性提昇，現在的網頁往往不單只是單純的網頁，而是一個網頁應用程式（ WebApp)。為了管理複雜的應用程式開發，此時模組化開發方法便顯得日益重要，而理想上的模組化開發工具一直是前端工程的很大的議題。
-   Webpack 和 Browserify + Gulp 則是進行 React 應用程式開發常用的開發工具，可以協助進行自動化程式碼打包、轉譯等重複性工作，提昇開發效率。
-   [Webpack](https://webpack.github.io/) 是一個模組打包工具（ module bundler）：
    - 將 CSS 、 圖片與其他資源打包。
    - 打包之前預處理（ Less、CoffeeScript、JSX、ES6 等）的檔案。
        - 依 entry 文件不同，把 .js 分拆為多個 .js 檔案。
        - 整合豐富的 Loader 可以使用（ Webpack 本身僅能處理 JavaScript 模組，其餘檔案如： CSS、Image 需要載入不同 Loader 進行處理）
-   Browserify _**lets you require('modules') in the browser by bundling up all of your dependencies.**_
    -   Browserify 是一個可以讓你在瀏覽器端也能使用像 Node 用的 [CommonJS](https://en.wikipedia.org/wiki/CommonJS) 規範一樣，用輸出（export）和引用（require）來管理模組。此外，也能讓前端使用許多在 NPM 中的模組。
-   Gulp
    -   一個前端任務工具自動化管理工具（Task Runner）
    -   減少開發過程中匯遇到重複工作的時間，已提高效率，例如：
        -   打包文件
        -   uglify
        -   將 LESS 轉譯成 css
        -   轉譯 ES6
        -   ....等

##### 延伸閱讀
- [參透 webpack](http://andyyou.logdown.com/posts/370326)
- [Grunt——前端任务自动管理工具](https://my.oschina.net/sevenhdu/blog/300951)

#### ESLint

- [ESLint](http://eslint.org/) 是一個提供 JavaScript 和 JSX 的程式碼檢查工具，可以確保團隊的程式碼品質。
- 根據需求在 `.eslintrc` 設定檢查規則。
- 目前主流的檢查規則會使用 Airbnb 所釋出的 [Airbnb React/JSX Style Guide](https://github.com/airbnb/javascript/tree/master/react)，在使用上需先安裝 [eslint-config-airbnb](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb) 等套件。

##### 延伸閱讀

- [ESLint 使用入门](https://csspod.com/getting-started-with-eslint/)

#### React Router

- [React Router](https://github.com/reactjs/react-router) 是 React 中主流使用的 Routing 函式庫，透過 URL 的變化來管理對應的狀態和元件。

##### 延伸閱讀

- [ReactTraining](https://github.com/ReactTraining/react-router)
- [[React Router 中文文档](https://github.com/react-guide/react-router-cn)](https://react-guide.github.io/react-router-cn/)
- [React Router 使用教程](http://www.ruanyifeng.com/blog/2016/05/react_router.html)

#### Flux / Redux

- Flux

  - 一個 Unidirectional Data Flow（單向流）的應用程式資料架構。
  - 由 Facebook 推出，並和 React 專注於 View 的部份形成互補。

- Redux

  - 核心概念：**只使用一個 store 將整個應用程式的狀態 (state) 用物件樹 (object tree) 的方式儲存起來。**

  - 原生的 Flux 會有許多分散的 store 儲存各個不同的狀態，但在 redux 中，只會有一個 store 將所有的資料用物件的方式包起來。

  - For example：

    ```javascript
    /*原生 Flux 的 Store*/
    	var firstStore = {first: 1};
    	var secondStore = {second: 2};
    /*Redux 的 單一 Store*/
    	var state = {
          	firstState: { first: 1 },
          	secondState: { second: 2 }
    	};
    ```

  - **唯一可以改變這個 state 的方法就是發送 action，這個 action 其實就只是一個物件告訴 state 該怎麼改變而已。**

  - **實際因應 action 裡的內容對 state 做變化的函式叫做 reducer。**

##### 延伸閱讀

- [Flux 架构入门教程](http://www.ruanyifeng.com/blog/2016/01/flux.html)
- [Redux 入門](https://rhadow.github.io/2015/07/30/beginner-redux/)
- [Redux 起手式：Actions、Reducers 及 Store](http://jigsawye.com/2015/11/29/instruction-with-redux/)

#### Immutable JS

- [ImmutableJS](https://facebook.github.io/immutable-js/)，是一個能讓開發者建立不可變資料結構的函式庫。建立不可變（immutable）資料結構不僅可以讓狀態可預測性更高，也可以提昇程式的效能。

##### 延伸閱讀

- [immutable-js](http://facebook.github.io/immutable-js/docs/#/)
- [Immutable.js 簡介](https://rhadow.github.io/2015/05/10/flux-immutable/)

#### Isomorphic JavaScript

- 根據 [Isomorphic JavaScript](http://isomorphic.net/) 這個網站的說明：

  - > [Isomorphic JavaScript](http://isomorphic.net/javascript) apps are JavaScript applications that can run both client-side and server-side.
    > The backend and frontend share the same code.

##### 延伸閱讀

- [Isomorphic JavaScript](http://isomorphic.net/javascript)
- [一看就懂的 React Server Rendering（Isomorphic JavaScript）入門教學](http://blog.techbridge.cc/2016/08/27/react-redux-immutablejs-node-server-isomorphic-tutorial/)

#### React 測試

- Facebook 本身有提供 [Test Utilities](https://facebook.github.io/react/docs/test-utils.html)，但由於不夠好用，所以目前主流開發社群比較傾向使用 Airbnb 團隊開發的 [enzyme](https://github.com/airbnb/enzyme)，其可以與市面上常見的測試工具（[Mocha](https://mochajs.org/)、[Karma](https://karma-runner.github.io/)、Jest 等）搭配使用。
- 其中 [Jest](https://facebook.github.io/jest/) 是 Facebook 所開發的單元測試工具，其主要基於 [Jasmine](http://jasmine.github.io/) 所建立的測試框架。Jest 除了支援 JSDOM 外，也可以自動模擬 (mock) 透過 `require()` 進來的模組，讓開發者可以更專注在目前被測試的模組中。

#### React Native

- [React Native](https://facebook.github.io/react-native/)和過去的 [Apache Cordova](https://cordova.apache.org/) 等基於 WebView 的解決方案比較不同，它讓開發者可以使用 React 和 JavaScript 開發原生應用程式（Native App），讓 `Learn once, write anywhere` 理想變得可能。

#### GraphQL / Relay

- [GraphQL](http://graphql.org/docs/getting-started/) 是 Facebook 所開發的，也是 Relay 用來做數據表示的查詢語言（Data Query Language），主要是想解決傳統 RESTful API 所遇到的一些問題，並提供前端更有彈性的 API 設計方式。
- [Relay](https://facebook.github.io/relay/) 則是 Facebook 提出搭配 GraphQL 用於 React 的一個宣告式數據框架，可以降低 Ajax 的請求數量（類似的框架還有 Netflix 推出的 [Falcor](https://netflix.github.io/falcor/)）。
- 但由於目前主流的後端 API 仍以傳統 RESTful API 設計為主，所以在使用 GraphQL 上通常會需要比較大架構設計的變動。

##### 延伸閱讀

- [Introduction to GraphQL](http://graphql.org/learn/)
- [Relay](https://facebook.github.io/relay/)
- [如何實作 GraphQL API](https://rhadow.github.io/2015/09/07/graphql-server-with-mongoose/)
- [使用 Relay 和 GraphQL](https://f8-app.liaohuqiu.net/tutorials/building-the-f8-app/relay/)

#### 其他延伸閱讀

1. [Navigating the React.JS Ecosystem](https://www.toptal.com/react/navigating-the-react-ecosystem)
2. [petehunt/react-howto](https://github.com/petehunt/react-howto#learning-relay-falcor-etc)
3. [React Ecosystem - A summary](https://staminaloops.github.io/undefinedisnotafunction/react-ecosystem/)
4. [React Official Site](https://facebook.github.io/react/)
5. [A collection of awesome things regarding React ecosystem](https://github.com/enaqx/awesome-react)
6. [Webpack 中文指南](http://zhaoda.net/webpack-handbook/index.html)
7. [AMD 和 CMD 的区别有哪些？](https://www.zhihu.com/question/20351507)
8. [jslint to eslint](https://www.qianduan.net/jslint-to-eslint/)
9. [Facebook的Web开发三板斧：React.js、Relay和GraphQL](http://1ke.co/course/595)
10. [airbnb/javascript](https://github.com/airbnb/javascript)

### 其它碎碎念

- [JavaScript Scoping and Hoisting](http://www.adequatelygood.com/JavaScript-Scoping-and-Hoisting.html)
- [[Javascript] 新手的困擾，Javascript 跟 JQuery 傻傻分不清楚](https://dotblogs.com.tw/maplenote/2014/07/21/146024)