# Redux 基礎概念
## Flux v.s. Reduc 
比較圖：
![dataflow](./using-redux-compare.jpg)

差異：

- 只使用一個 store 將整個 Application 的 state 用 Object tree 的方式儲存起來：

  - Native Flux 會有許多分散的 store 來儲存各個不同的狀態。
  - Redux 中，只會有唯一一個 store 將所有的 state 用 object tree 的方式包起來。 

  ```javascript
  // Native Flux
  const userStore = {
  	name: ''
  }
  const todoStore = {
  	text: ''
  }

  // Redux Single Store
  const state = {
  	userState: {
       	name: ''
  	},
      todoState: {
  		text: ''
      }
  }
  ```

- 唯一可以改變 state 的方法就是送出 action，這部份和 **Flux** 類似，但 Redux 並沒有像 Flux 設計有 Dispatcher。

  - Redux 的 action 和 Flux 的 action 都是一個包含 `type` 和 `payload` 的物件。

- Redux 有 **Reducer**。

  - 根據 action 的 type 去執行對應的 state 做變化的函式叫做「 Reducer 」。
  - 可以使用「 switch 」、「 mapping 」。

- 輔助測試工具：

  - [redux-devtools](https://github.com/gaearon/redux-devtools)
  - [react-transform-boilerplate](https://github.com/gaearon/react-transform-boilerplate)
  - 方便測試和使用 **`Hot Module Reload`**。

## Redux 核心概念介紹

![redux-flowchart.png](./redux-flowchart.png)

- Redux Data Flow 大概是： `View -> Action -> (Middleware) -> Reducer`。
  - When User interactive with View, 他會觸發事件發出 Action。
  - 若有使用 Middleware 的話會在進入 Reducer 進行一些處理。
  - 當 Action 進到 Reducer時，Reducer 會根據 Action Type 去 Mapping 對應處理的動作，然後回傳新的 State。
  - View 則因為偵測到 State 更新而重新 Render。

## Redux API 入門

- createStore：`createStore(reducer, [preloadedState], [enhancer])`

  - 在產生 store 時我們會使用 `createStore` 這個 API 來創建 store。
  - 第一個參數放入我們的 `reducer` 或是有多個 `reducers` combine（使用 `combineReducers`）在一起的 `rootReducers`。
  - 第二個參數選擇性放入預先載入的 `state`。
    - 例如： user session ...等。
  - 第三個參數通常會放入用來增強的 Redux 功能的 `middlewares`。
    - 若有多個 middlewares，，通常會使用 `applyMiddleware` 來整合。

- Store，四個方法：

  Store 重點是要知道 Redux 只有一個 Store 負責存放整個 App 的 State，而唯一能改變 State 的方法只有發送 Action。

  - getState()
  - dispatch(action)
  - subscribe(listener)
  - replaceReducer(nextReducer)

- combineReducers：`combineReducers(reducers)`

  - combineReducers 可以將多個 reducers 進行整合並回傳一個 Function，達到適度切割 reducer。

- applyMiddleware：`applyMiddleware(...middlewares)`

  - middleware 讓開發者可以在 req 和 res 之間進行一些操作。
  - Redux 中 Middleware 則是扮演 action 到達 reducer 前的第三方擴充。
  - 而 applyMiddleware 可以將多個 `middlewares`整合並回傳一個 Function，便於使用。
  - 若是你要使用 asynchronous 的行為的話需要使用其中一種 middleware：
    - [redux-thunk](https://github.com/gaearon/redux-thunk)
    - [redux-promise](https://github.com/acdlite/redux-promise)
    - [redux-promise-middleware](https://github.com/pburtchaell/redux-promise-middleware) 

  ![react-redux-diagram](./react-redux-diagram.png)

- bindActionCreators：`bindActionCreators(actionCreators, dispatch)`

  - bindActionCreators 可以將 `actionCreators` 和 `dispatch` 綁定。
  - 回傳一個 Function or Object。
  - 但是若使用 **`react-redux`** 可以用 `connect` 讓 dispatch 行為更容易管理。

- compose：`compose(...functions)`

  - compose 可以將 function 由右到左合併並回傳一個 Function。

## 延伸閱讀

- [React AJAX Best Practices](http://andrewhfarmer.com/react-ajax-best-practices/)
- [Redux 官方網站](http://redux.js.org/index.html)
- [Redux架构实践——Single Source of Truth](http://react-china.org/t/redux-single-source-of-truth/5564)
- [Presentational and Container Components](https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0)
- [使用Redux管理你的React应用](https://github.com/matthew-sun/blog/issues/18)
- [Using redux](http://www.slideshare.net/JonasOhlsson/using-redux)

# Redux 實戰入門

## React Redux App 的資料流程圖

使用者與 View 互動 => dispatch 出 Action => Reducers 依據 action type 分配到對應處理方式，回傳新的 state => 透過 React Redux 傳送給 React，React 重新繪製 View：

![redux-flow](./redux-flow.png)



## react-redux 用法

 `react-redux` 是 React 和 Redux 間的橋樑，使用 `Provider`、`connect` 去連結 `store` 和 React View。

![using-redux](./using-redux.jpg)

整合 react-redux 後， React App 就可以解決傳統跨 Component 之前傳遞 state 的問題和困難。

只要透過 `Provider` 就可以讓每個 React App 中的 `Component` 取用 store 中的 state，非常方便

![redux-store.png](./redux-store.png)

## 延伸閱讀

- [大白话讲解Promise（一）](http://www.cnblogs.com/lvdabao/p/es6-promise-1.html)
- [大白话讲解Promise（二）理解Promise规范](http://www.cnblogs.com/lvdabao/p/5320705.html)
- [大白话讲解Promise（三）搞懂jquery中的Promise](http://www.cnblogs.com/lvdabao/p/jquery-deferred.html)