# Props、State、Refs 與表單處理

在前面的介紹，我們了解到 React Component 事實上可以視為顯示 UI 的一個狀態機（state machine），而這個狀態機根據不同的 state（透過 `setState()` 修改）和 props（由父元素傳入），Component 會出現對應的顯示結果。

## Props

app.js，使用 ES6 Class Component 寫法：

```javascript
import React from 'react';
import ReactDOM from 'react-dom';

class HelloMessage extends React.Component {
    // 若是需要綁定 this.方法或是需要在 constructor 使用 props，定義 state，就需要 constructor。
    // 若是在其他方法（如 render）使用 this.props 則不用一定要定義 constructor
    constructor(props) {
        // 對於 OOP 物件導向程式設計熟悉的讀者應該對於 constructor 建構子的使用不陌生，
        // 事實上它是 ES6 的語法糖，骨子裡還是 prototype based 物件導向程式語言。
        // 透過 extends 可以繼承 React.Component 父類別。super 方法可以呼叫繼承父類別的建構子
        super(props);
        this.state = {}
    }
    // render 是唯一必須的方法，但如果是單純 render UI 建議使用 Functional Component 寫法，效能較佳且較簡潔
    render() {
        return (
            <div>Hello {this.props.name}</div>
        );
    }  
}
// PropTypes 驗證，若傳入的 props type 不是 string 將會顯示錯誤
HelloMessage.propTypes = {
  name: React.PropTypes.string,
}

// Prop 預設值，若對應 props 沒傳入值將會使用 default 值 Zuck
HelloMessage.defaultProps = {
 name: 'Zuck',
}

ReactDOM.render(<HelloMessage />, document.getElementById('app1'));
```

關於 React ES6 class constructor super() 解釋可以參考 [React ES6 class constructor super()](http://cheng.logdown.com/posts/2016/03/26/683329) 。

Functional Component 寫法：

```javascript
import React from 'react';
import ReactDOM from 'react-dom';
// Functional Component 可以視為 f(d) => UI，根據傳進去的 props 繪出對應的 UI。
// 注意這邊 props 是傳入函式的參數，因此取用 props 不用加 this
const HelloMessage = (props) => (
    <div>Hello {props.name}</div>
);

ReactDOM.render(<HelloMessage name="Mark" />, document.getElementById('app'));
```

官方文件：https://facebook.github.io/react/docs/components-and-props.html

## State

接下來將使用 [A Stateful Component](https://facebook.github.io/react/#timerExample) 這個範例來講解 State 的用法。

- 在 React Component 可以自己管理自己的內部 state，並用 `this.state` 來存取 state。

- 當 `setState()` 方法更新了 state 後將重新呼叫 `render()` 方法，重新繪製 component 內容。

- 以下範例是一個每 1000 毫秒（等於1秒）就會加一的累加器。

  - 由於這個範例是 Stateful Component 因此僅使用 ES6 Class Component，而不使用 Functional Component。

  ```javascript
  import React from 'react';
  import ReactDOM from 'react-dom';

  class Timer extends React.Component {
      constructor(props) {
          super(props);
          // 與 ES5 React.createClass({}) 不同的是 component 內自定義的方法
        	// 需要自行綁定 this context，或是使用 arrow function
          this.tick = this.tick.bind(this);
          // 初始 state，等於 ES5 中的 getInitialState
          this.state = {
              secondsElapsed: 0,
          }
      }
      // 累加器方法，每一秒被呼叫後就會使用 setState() 更新內部 state，讓 Component 重新 render
      tick() {
          this.setState({secondsElapsed: this.state.secondsElapsed + 1});
      }
      // componentDidMount 為 component 生命週期中階段 component 已插入節點的階段，通常一些非同步操作都會放置在這個階段。這便是每1秒鐘會去呼叫 tick 方法
      componentDidMount() {
          this.interval = setInterval(this.tick, 1000);
      }
      // componentWillUnmount 為 component 生命週期中 component 即將移出插入的節點的階段。這邊移除了 setInterval 效力
      componentWillUnmount() {
          clearInterval(this.interval);
      }
      // render 為 class Component 中唯一需要定義的方法，其回傳 component 欲顯示的內容
      render() {
          return (
            <div>Seconds Elapsed: {this.state.secondsElapsed}</div>
          );
      }
  }

  ReactDOM.render(<Timer />, document.getElementById('app'));
  ```

- [Stateful Component](https://rangle-io.gitbooks.io/react-training/content/book/react_components/stateful.html) 參考文件

- 關於 Javascript this 用法可以參考 [Javascript：this用法整理](https://software.intel.com/zh-cn/blogs/2013/10/09/javascript-this)。

## Event Handle
React [An Application 範例](https://facebook.github.io/react/#todoExample)
```javascript
import React from 'react';
import ReactDOM from 'react-dom';
// TodoApp 元件中包含了顯示 Todo 的 TodoList 元件，Todo 的內容透過 props 傳入 TodoList 中。
// 由於 TodoList 僅單純 Render UI 不涉及內部 state 操作是 stateless component，
// 所以使用 Functional Component 寫法。需要特別注意的是這邊我們用 map function 來迭代 Todos，
// 需要留意的是每個迭代的元素必須要有 unique key 不然會發生錯誤
// （可以用自定義 id，或是使用 map function 的第二個參數 index）
const TodoList = (props) => (
	<ul>
  		{
          props.items.map((item) => (
          	<li key={item.id}>{item.text}</li>
          ))
  		}
  	</ul>
)

// 整個 App 的主要元件，比較重要的是事件處理的部份，內部有
class TodoApp extends React.Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            items: [],
            text: '',
        }
    }
    onChange(e) {
        this.setState({text: e.target.value});
    }
    handleSubmit(e) {
        e.preventDefault();
        const nextItems = this.state.items.concat([{text: this.state.text, id: Date.now()}]);
        const nextText = '';
        this.setState({items: nextItems,text: nextText});
    }
    render() {
        return (
            <div>
                <h3> ToDo </h3>
                <TodoList items={this.state.items} />
                <form onSubmit={this.handleSubmit}>
                    <input onChange={this.onChange} value = {this.state.text} />
                    <button>{'Add #' + (this.state.items.length + 1)}</button>
                </form>
            </div>
        );
    }
}
```
- React 事件處理的部份，除了 `onChange` 和 `onSubmit` 外，React 也封裝了常用的事件處理，如 `onClick` 等。
- 更多可以使用的事件處理方法可以參考
  -  [官網的 Event System](https://facebook.github.io/react/docs/events.html)
  -  [Handling Events](https://facebook.github.io/react/docs/handling-events.html)

## Refs 與表單處理

前面介紹了 **` props（傳入後就不能修改）`**、**` state（隨著使用者互動而改變 `**、**` 事件處理機制 `** 後，在這裡要介紹如何在 React 中進行表單處理。

透過官網範例 [ A Component Using External Plugins](https://facebook.github.io/react/#markdownExample) ，React 可以容易整合外部的 libraries（例如：jQuery），本範例將使用 `remarkable` 結合 `ref` 屬性取出 DOM Value 值（另外比較常用的作法是使用 `onChange` 事件處理方式處理表單內容），讓使用者可以使用 Markdown 語法的所見即所得 editor。

```javascript
import React from 'react';
import ReactDOM from 'react-dom';
import Remarkable from 'remarkable';

class MarkdownEditor extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.rawMarkup = this.rawMarkup.bind(this);
        this.state = {
            value: 'Type some *markdown* here!',
        }
    }
    handleChange() {
        this.setState({value: this.refs.aaa.value});
    }
    // 將使用者輸入的 Markdown 語法 parse 成 HTML 放入 DOM 中，React 通常使用 virtual DOM 作為和 DOM 溝通的中介，
    // 不建議直接由操作 DOM。故使用時的屬性為 dangerouslySetInnerHTML
    rawMarkup() {
        const md = new Remarkable();
        return { __html: md.render(this.state.value) };
    }
    render() {
        return(
            <div className="MarkdownEditor">
                <h3>Input</h3>
                <textarea
                    onChange={this.handleChange}
                    ref="aaa"
                    defaultValue={this.state.value}/>
                <h3>Output</h3>
                <div 
                    className="content"
                    dangerouslySetInnerHTML={this.rawMarkup()}/>
            </div>
        );
    }
}
```

- [ReactDOM.render返回的ref引用](http://bbs.reactnative.cn/topic/608/%E5%AF%B9%E7%BB%84%E4%BB%B6%E7%9A%84%E5%BC%95%E7%94%A8-refs)
- [React Official Document - Refs and the DOM](https://facebook.github.io/react/docs/refs-and-the-dom.html)

# React Component 規格與 Life Cycle

## React Component 規格

前面介紹 React 特性時有描述 React 的主要撰寫方式有兩種：

- 一種是使用 ES6 Class
- 另外一種是 Stateless Components ( Functional Component 寫法)，單純 render UI。

### ES6 Class

> 可以進行比較複雜的操作和生命週期的控制，相對於 stateless components 耗費資源

```javascript
class MyComponent extends React.Component {
  render() {
    return (
    	<div> Hello, {this.props.name} </div>
    );
  }
}

MyComponent.propTypes = {
  name: React.PropTypes.string,
}

MyComponent.defaultProps = {
  name: '',
}

ReactDOM.render(<MyComponent name="Mark" />, document.getElementById('app'));
```

### Functional Component

> - 純粹 render UI  的 stateless components，沒有內部狀態、沒有實作物件和 ref，更沒有生命週期函數。
> - 建議若非需要控制生命週期的話，多使用 stateless components 可以獲得較好的效能。

```javascript
const MyComponent = (props) => (
	<div> Hello, {props.name} </div>
);
MyComponent.propTypes = {
  name: React.PropTypes.string,
}
MyComponent.defaultProps = {
  name: '',
}
ReactDOM.render(<MyComponent name="Mark" />, document.getElementById('app'));
```

### Notice!!!

值得注意的是在 ES6 Class 中 render() 是唯一必要的方法，** 但要注意的是請保持 render() 內部的單純化，不需要在裡面進行 state 的修改 或是 使用非同步方法和瀏覽器互動 **，若需要非同步互動請在 componentDidMount() 內操作。

而 Functional Component 目前允許 return null 。

另外值得一提的是，[在 ES6 中不支援 mixins 複用其他元件的方法](https://facebook.github.io/react/blog/2016/07/13/mixins-considered-harmful.html)。

## React Component 生命週期

- React Component 就像人會有生老病死一樣有一個生命週期。

- 一般而言，Component 有以下三種生命週期的狀態：
  - Mounting：已插入真實的 DOM。
  - Updating：正在被重新渲染。
  - Unmounting：已移出真實的 DOM。

- 針對上述所說的三種生命週期，React 有提供相對應的處理方法：
  - Mouting：
    - componentWillMount()
    - componentDidMount()
  - Updating
    - componentWillReceiveProps(**object** nextProps)
      - 已載入元件收到新的參數時呼叫。
    - shouldComponentUpdate(**object** nextProps, **object** nextState)
      - 元件判斷是否重新 render 時呼叫，起始不會呼叫。
      - 除非呼叫 forceUpdate()。
    - componentWillUpdate(**object** nextProps, **object** nextState)
    - componentDidUpdate(**object** prevProps, **object** prevState)
  - Unmounting
    - componentWillUnmount()

- 先來張圖片

  - ![react-lifecycle](./react-lifecycle.png)

  - Component 生命週期展示：

    ```javascript
    class ReactCycleLife extends React.Component {
        constructor(props) {
            super(props);
            console.log('constructor');
            this.handleClick = this.handleClick.bind(this);
            this.state = {
                name: 'Mark',
            }
        }
        handleClick() {
            this.setState({'name': 'Zuck'});
        }
        componentWillMount() {
            console.log('componentWillMount');
        }
        componentDidMount() {
            console.log('componentDidMount');
        }
        componentWillReceiveProps() {
            console.log('componentWillReceiveProps');
        }
        componentWillUpdate() {
            console.log('componentWillUpdate');
        }
        componentDidUpdate() {
            console.log('componentDidUpdate');
        }
        componentWillUnmount() {
            console.log('componentWillUnmount');
        }
        render() {
            return(
                <div onClick={this.handleClick}>
                    Hi, {this.state.name}
                </div>
            );
        }
    }
    ```

- 其中特殊處理的函數 `shouldComponentUpdate`，目前預設 `return true`。

  - 你想要優化效能可以自己編寫判斷方式，若採用 `immutable` 可以使用 **[is()](https://facebook.github.io/immutable-js/docs/#/is)**

    ```javascript
    is(first: any, second: any): boolean
    ```

    比對是否有變動：

    ```javascript

      const MyComponent extends Component {
          constructor(props){
              super(props);
              this.state = {
                data: Map({
                  _item: '',
                }),
              };
          }
          componentDidMount() {
          	this.setState(({data}) => ({
              data: data.update('_item', v => '123'),
          	}));
          }
         /* 判斷 state 不一樣就重新 Render */ 
          shouldComponentUpdate(nextProps, nextState) {
            if(!is(this.state.data, nextState.data)) {
              return true
            }
            return false;
          }
      }
      
    ```

## Ajax 非同步處理

- 因為 React 只負責處理 View 這一層，它本身不涉及網路請求 / AJAX，所以根據 [React 最佳实践——那些 React 没告诉你但很重要的事](https://segmentfault.com/a/1190000005013207) 文章所提到的，我們需要考慮兩個問題：
  - 第一：我們要用什麼技術從 Server 獲取數據。
  - 第二：獲取到的數據應該放在 React Component 的哪個地方。
- 在 React 官方網站，提供了一個 Solution：[Load Initial Data via AJAX](http://reactjs.cn/react/tips/initial-ajax.html)
  - 在 `componentDidMount()` 中使用 jQuery 的 AJAX 方法發出 AJAX 請求，拿到的數據存在 Component 自己的 state 中，並調用 setState 方法來更新 UI。
  - 如果是 Async 獲取數據，則在 `componentWillUnmount` 中取消發送請求。
- 三種較好的實作：
  - 所有數據請求和管理都存放在唯一的一個 Root Component
  - **設置多個 Container Component 專門處理數據請求和管理**
  - 使用 Redux 或 Relay 的情況
    - Redux 管理狀態和數據， AJAX 從 Server 獲取數據，所以當我們用 Redux 時，應該把所有的請求都交給 Redux 來解決。
    - 應該放在 [Async Actions](http://redux.js.org/docs/advanced/AsyncActions.html) 。